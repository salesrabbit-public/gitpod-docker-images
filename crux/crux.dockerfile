FROM gitpod/workspace-base

ARG ELIXIR_VERSION=1.13.4
ARG ERLANG_VERSION=25.0
ARG NODE_VERSION=12.22.5

RUN bash -c "sudo apt update && \
    sudo apt install -y curl git inotify-tools docker-ce docker-ce-cli  && \
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.9.0 && \
    echo '. $HOME/.asdf/asdf.sh' >> ~/.bashrc && \
    echo '. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc"

# Install asdf, erlang, elixir and nodejs
RUN bash -c "source $HOME/.asdf/asdf.sh && asdf plugin add erlang && \
    asdf plugin add elixir && \
    asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git && \
    asdf install erlang ${ERLANG_VERSION} && \
    asdf install elixir ${ELIXIR_VERSION} && \
    asdf install nodejs ${NODE_VERSION}"

# Install db clients
RUN sudo apt update && sudo apt install -y mysql-client postgresql-client

# Set up elixir and mix
RUN bash -c "source $HOME/.asdf/asdf.sh && \
    asdf global erlang ${ERLANG_VERSION} && \
    asdf global elixir ${ELIXIR_VERSION} && \
    mix local.hex --force && \
    mix local.rebar --force"
