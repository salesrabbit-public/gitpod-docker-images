FROM base 

RUN apt update && \
    apt upgrade -y libgdal-dev tini jq postgresql-common awscli && \
    /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh -y && \
    apt update && \
    apt upgrade -y postgresql-16 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* && \
    /usr/bin/curl -sL https://sentry.io/get-cli/ | SENTRY_CLI_VERSION="1.47.1" sh && \
    useradd app -m && \
    chsh app -s /bin/bash && \
    chown -R app:app /app

USER app
WORKDIR /app
ENV PATH="/home/app/.local/bin:/app/bin:$PATH"
ENV PYTHONPATH="/app/src"

RUN mkdir -p /app/var /app/public/assets /app/public/media && \
    pip install --no-cache-dir pip-tools awscli && \
    pip install --no-cache-dir -r requirements.txt && \
    python src/manage.py check && \
    python src/manage.py collectstatic --noinput --no-color --traceback --verbosity=1 --ignore ".cache" --ignore "node_modules" --ignore "Zurb" --ignore "src"
