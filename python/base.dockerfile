FROM python:3.7-slim-bookworm as base

ENV LC_COLLATE=C \
    TERM="xterm-256color" \
    DEBIAN_FRONTEND=noninteractive \
    PYTHONBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_TIMEOUT=60 \
    PIP_DISABLE_PIP_VERSION_CHECK=true

RUN set -eux; \
    \
    export GOSU_VERSION=1.10; \
    export TINI_VERSION=0.18.0; \
    fetchDeps='ca-certificates wget dirmngr gnupg cabextract xfonts-utils'; \
    savedAptMark="$(apt-mark showmanual)"; \
    dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
# Sometimes keyservers are not available, we will try several until one works.
    keyservers=" \
        ha.pool.sks-keyservers.net \
        hkp://p80.pool.sks-keyservers.net:80 \
        keyserver.ubuntu.com \
        hkp://keyserver.ubuntu.com:80 \
        pgp.mit.edu \
    "; \
    \
    echo deb http://deb.debian.org/debian bookworm main contrib non-free > /etc/apt/source.list ; \
    echo deb http://deb.debian.org/debian bookworm-updates main contrib non-free >> /etc/apt/source.list ; \
    echo deb http://security.debian.org bookworm/updates main >> /etc/apt/source.list ; \
    apt-get update; \
    apt-get install -y --no-install-recommends $fetchDeps; \
    rm -rf /var/lib/apt/lists/*; \
    \
# grab gosu for easy step-down from root
# https://github.com/tianon/gosu/releases
    wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-$dpkgArch"; \
    wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-$dpkgArch.asc"; \
    export GNUPGHOME="$(mktemp -d)"; \
    for server in ${keyservers}; do \
       gpg --no-tty --batch --keyserver "${server}" --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && break || : ; \
    done; \
    gpg  --no-tty --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
    gpgconf --kill all; \
    rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc; \
    chmod +x /usr/local/bin/gosu; \
    gosu nobody true; \
    \
# grab tini for signal processing and zombie killing
# https://github.com/krallin/tini/releases
    wget -O /usr/local/bin/tini "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini-$dpkgArch"; \
    wget -O /usr/local/bin/tini.asc "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini-$dpkgArch.asc"; \
    export GNUPGHOME="$(mktemp -d)"; \
    for server in ${keyservers}; do \
        gpg --no-tty --batch --keyserver "${server}" --recv-keys 595E85A6B1B4779EA4DAAEC70B588DFF0527A9B7 && break || : ; \
    done; \
    gpg --no-tty --batch --verify /usr/local/bin/tini.asc /usr/local/bin/tini; \
    gpgconf --kill all; \
    rm -r "$GNUPGHOME" /usr/local/bin/tini.asc; \
    chmod +x /usr/local/bin/tini; \
    tini -vvv -- bash -c 'true'; \
    \
# Install MS Fonts, needed to generate useful PDF's
    wget -q -O ttf-mscorefonts-installer_3.7_all.deb http://http.us.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.7_all.deb; \
    dpkg -i ttf-mscorefonts-installer_3.7_all.deb; \
    rm -rf ttf-mscorefonts-installer_3.7_all.deb; \
    \
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
    apt-mark auto '.*' > /dev/null; \
    [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false

# Install system libraries
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
# Common tools for development and building
        openssh-client \
        make \
        build-essential \
        gcc \
        clang-format \
        binutils \
        git \
        gettext \
        tzdata \
        ca-certificates \
        locales \
# Postgis libs
        gdal-bin \
        libproj-dev \
# Requirements for generating pdf thumbnails
        imagemagick \
        ghostscript \
# Library headers to build python packages
        libpython3-dev \
        musl-dev \
        zlib1g-dev \
        libc6-dev \
        libpq-dev \
        libxml2-dev \
        libxslt1-dev \
        libjansson-dev \
        libpcre3-dev \
        libpng-dev \
        libfreetype6-dev \
        libjpeg-dev \
        libssl-dev \
        zlib1g-dev \
        libbz2-dev \
        liblzma-dev \
        libffi-dev \
        libheif-dev \
        libde265-dev; \
    \
    rm -rf /var/lib/apt/lists/*;

RUN set -eux; \
    \
    echo en_US.UTF-8 UTF-8 > /etc/locale.gen; \
    locale-gen en_US.UTF-8;

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

# Setup non-root user
RUN set -eux; \
    \
    addgroup --gid 1000 app; \
    adduser --uid 1000 \
        --gid 1000 \
        --no-create-home \
        --disabled-password --quiet app;

# Setup non-root user
RUN set -eux; \
    \
    mkdir -p /app; \
    mkdir -p /python; \
    chown -R app /app; \
    chown -R app /python;

ENV PATH="/python/bin:${PATH}" \
    XDG_CACHE_HOME="/python/cache" \
    DEFAULT_LOCAL_TMP="/python/cache" \
    PYTHONPATH="/app/src" \
    PYTHONENV="/python"

WORKDIR "/app"

EXPOSE 8000

FROM base as development

# TODO: Copy python tools: curl.py

RUN set -eux; \
    \
# Documentation folders if missing will make apt-get fail
    mkdir -p /usr/share/man/man1; \
    mkdir -p /usr/share/man/man7; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        sudo \
        htop \
        groff \
        vim \
        tmux \
        curl \
        unzip \
        gnupg; \
\
# Installing pg_restore, pg_dump, psql from the the postgres repository
    \
    echo "deb http://apt.postgresql.org/pub/repos/apt/ bookworm-pgdg main" | tee -a /etc/apt/sources.list; \
    curl -o ACCC4CF8.asc "https://www.postgresql.org/media/keys/ACCC4CF8.asc"; \
    apt-key add ACCC4CF8.asc; \
    rm -f ACCC4CF8.asc; \
    \
    apt-get update --quiet; \
    apt-get install -y --no-install-recommends \
        postgresql-client; \
    \
    rm -rf /var/lib/apt/lists/*;

# Deploy dependencies

RUN set -eux; \
    \
# Install aws-cli
    \
    /usr/local/bin/pip install --quiet --no-cache-dir \
        awscli; \
    which aws; \
    \
# Install ecs-cli
    \
    curl --silent \
        -o /usr/local/bin/ecs-cli \
        https://s3.amazonaws.com/amazon-ecs-cli/ecs-cli-linux-amd64-latest; \
    chmod +x /usr/local/bin/ecs-cli;

# Install docker tools
RUN set -eux; \
    export DOCKER_VERSION=20.10.6; \
    \
    curl --silent \
        -o docker-${DOCKER_VERSION}.tgz \
        https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz; \
    tar -xzf docker-${DOCKER_VERSION}.tgz; \
    mv docker/docker /usr/local/bin/docker; \
    chmod +x /usr/local/bin/docker; \
    rm docker-${DOCKER_VERSION}.tgz; \
    which docker; \
    rm -r docker/; \
# Install docker-compose
    \
    curl --silent \
        -o /usr/local/bin/docker-compose \
        "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-Linux-x86_64"; \
    chmod +x /usr/local/bin/docker-compose; \
    which docker-compose;

RUN set -eux; \
# Install ansible
    \
    /usr/local/bin/pip install --prefix=/usr/local --no-cache-dir --upgrade wheel setuptools pip; \
    /usr/local/bin/pip install --prefix=/usr/local --no-cache-dir ansible; \
    which ansible;

