FROM gitpod/workspace-full

RUN sudo apt update && \
    sudo apt install -y curl git inotify-tools  && \
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.9.0 && \
    echo '. $HOME/.asdf/asdf.sh' >> ~/.bashrc && \
    echo '. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc

# Install asdf, erlang, elixir and nodejs
RUN bash -c "source $HOME/.asdf/asdf.sh && asdf plugin add erlang && \
    asdf plugin add elixir && \
    asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git && \
    asdf install erlang 24.0.2 && \
    asdf install elixir 1.12.3 && \
    asdf install nodejs 12.22.5"

# Set up elixir and mix
RUN bash -c "source $HOME/.asdf/asdf.sh && asdf global erlang 24.0.2 && asdf global elixir 1.12.3 && mix local.hex --force && mix local.rebar --force"
