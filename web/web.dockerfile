FROM gitpod/workspace-base
ARG DEBIAN_FRONTEND=noninteractive

RUN sudo apt update && \
    sudo apt install -y curl git inotify-tools docker-ce docker-ce-cli  && \
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.9.0 && \
    echo '. $HOME/.asdf/asdf.sh' >> ~/.bashrc && \
    echo '. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc

RUN bash -c "source $HOME/.asdf/asdf.sh && \
    asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git && \
    asdf install nodejs 14.20.0"
