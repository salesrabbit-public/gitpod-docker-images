FROM --platform=linux/amd64 gitpod/workspace-base
ARG DEBIAN_FRONTEND=noninteractive

RUN sudo apt update && \
  sudo apt install python3.8 && \
  sudo apt install -y python3-pip && \
  sudo pip3 install -U Commitizen && \
  sudo apt install -y curl git inotify-tools docker-ce docker-ce-cli libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb procps && \
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.9.0 && \
  echo '. $HOME/.asdf/asdf.sh' >> ~/.bashrc && \
  echo '. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc

RUN bash -c "source $HOME/.asdf/asdf.sh && \
  asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git && \
  asdf install nodejs 18.12.1 && \
  asdf install nodejs 14.20.0 && \
  asdf global nodejs 18.12.1 && \
  asdf plugin-add yarn && \
  asdf install yarn 1.22.19 && \
  asdf plugin-add pnpm && \
  asdf install pnpm 7.15.0 && \
  npm install --global cypress@12.7.0"
