FROM gitpod/workspace-base
ARG DEBIAN_FRONTEND=noninteractive

# Install required libs
RUN sudo mkdir -p /usr/src/php && \
    sudo apt-get update && \
    sudo apt-get install -y \
    curl \
    default-libmysqlclient-dev \
    default-mysql-client \
    docker-ce \
    docker-ce-cli \
    git \
    iproute2 \
    libcurl4-gnutls-dev \
    libjpeg-dev \
    libjson-c-dev \
    libmcrypt-dev \
    libmemcached-dev \
    libpng-dev \
    libxml2-dev \
    memcached \
    unzip \
    vim \
    zip \
    zlib1g \
    zlib1g-dev \
    zlibc && \
    sudo apt-get install -y software-properties-common dialog apt-utils && \ 
    sudo add-apt-repository -y ppa:ondrej/php && \
    sudo apt-get update && \
    sudo apt-get install -y php7.1 php7.1-gd php7.1-mbstring php7.1-soap php7.1-xml php7.1-json php7.1-curl php7.1-opcache php7.1-dev php7.1-xml php7.1-mysql php7.1-zip && \
    sudo chown -R gitpod:gitpod /var/log/apache2 && \
    sudo chown -R gitpod:gitpod /var/run/apache2 && \
    sudo sed -i '/^Listen/ s/80/3000/' /etc/apache2/ports.conf

# Configure PHP
RUN sudo cp /usr/lib/php/7.1/php.ini-development /etc/php/7.1/cli/php.ini && \
    sudo cp /usr/lib/php/7.1/php.ini-development /etc/php/7.1/apache2/php.ini

# Install required PHP extensions and enable modules in Apache
RUN sudo pecl install xdebug-2.9.1 && \
    echo zend_extension=/usr/lib/php/20160303/xdebug.so | sudo tee -a /etc/php/7.1/apache2/php.ini /etc/php/7.1/cli/php.ini && \
    CFLAGS="-fgnu89-inline" sudo pecl install -f memcache-4.0.5.2 && \
    echo extension=memcache.so | sudo tee -a /etc/php/7.1/apache2/php.ini /etc/php/7.1/cli/php.ini && \
    sudo pecl install redis && \
    echo extension=redis.so | sudo tee -a /etc/php/7.1/apache2/php.ini /etc/php/7.1/cli/php.ini && \
    sudo a2enmod rewrite && \
    sudo a2enmod negotiation

# Configure Memcache
COPY dev.php.ini /etc/php/7.1/cli/conf.d/
COPY dev.php.ini /etc/php/7.1/apache2/conf.d/
COPY custom-xdebug.ini /etc/php/7.1/apache2/conf.d/

# ENV APACHE_DOCUMENT_ROOT /var/www/html/public
RUN sudo sed -r -i -e 's,DocumentRoot .*$,DocumentRoot /workspace/data-layer/public,' -e '/VirtualHost/ s/\*:80/*:3000/' /etc/apache2/sites-available/*conf && \
    sudo sed -r -i -e '/Directory/ s,/var/www/,/workspace/data-layer/public,' -e '/AllowOverride/ s/None/All/' /etc/apache2/apache2.conf
