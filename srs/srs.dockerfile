FROM gitpod/workspace-base
ARG DEBIAN_FRONTEND=noninteractive

RUN sudo mkdir -p /var/setup && sudo mkdir -p /var/settings
# Install required libs
RUN sudo mkdir -p /usr/src/php && \
    sudo apt-get update && \
    sudo apt-get install -y \
    curl \
    docker-ce \
    docker-ce-cli \
    ffmpeg \
    git \
    gpac \
    iproute2 \
    libjpeg-dev \
    libjson-c-dev \
    libmcrypt-dev \
    libmemcached-dev \
    libpng-dev \
    libxml2-dev \
    memcached \
    mysql-client \
    netcat \
    unzip \
    vim \
    zlib1g-dev 

RUN sudo apt-get install -y software-properties-common dialog apt-utils && \ 
    sudo add-apt-repository -y ppa:ondrej/php && \
    sudo apt-get update && \
    sudo apt-get install -y php5.6 php5.6-gd php5.6-mbstring php5.6-soap php5.6-xml php5.6-json php5.6-curl php5.6-opcache php5.6-dev php5.6-xml php5.6-mysql php5.6-zip && \
    sudo chown -R gitpod:gitpod /var/log/apache2 && \
    sudo chown -R gitpod:gitpod /var/run/apache2 && \
    sudo sed -i '/^Listen/ s/80/3000/' /etc/apache2/ports.conf

# Configure PHP
RUN sudo cp /usr/lib/php/5.6/php.ini-development /etc/php/5.6/cli/php.ini && \
    sudo cp /usr/lib/php/5.6/php.ini-development /etc/php/5.6/apache2/php.ini

RUN sudo pear channel-discover pear.phing.info && sudo pear install phing/phing Log

# Install required PHP extensions and enable modules in Apache
RUN sudo pecl install xdebug-2.5.5 

RUN echo zend_extension=/usr/lib/php/20131226/xdebug.so | sudo tee -a /etc/php/5.6/apache2/php.ini /etc/php/5.6/cli/php.ini

RUN sudo CFLAGS="-fgnu89-inline" pecl install memcache-3.0.8 && \
    echo extension=memcache.so | sudo tee -a /etc/php/5.6/apache2/php.ini /etc/php/5.6/cli/php.ini && \
    sudo pecl install channel://pecl.php.net/redis-2.2.8 && \
    sudo pecl install runkit && \
    sudo a2enmod rewrite && \
    sudo a2enmod negotiation

# # Configure Memcache
COPY dev.php.ini /etc/php/5.6/cli/conf.d/
COPY dev.php.ini /etc/php/5.6/apache2/conf.d/
COPY custom-xdebug.ini /etc/php/5.6/apache2/conf.d/

# ENV APACHE_DOCUMENT_ROOT /var/www/html/docroot
RUN sudo sed -r -i -e 's,DocumentRoot .*$,DocumentRoot /workspace/legacy-server/docroot,' -e '/VirtualHost/ s/\*:80/*:3000/' /etc/apache2/sites-available/*conf && \
    sudo sed -r -i '/Directory/ s,/var/www/,/workspace/legacy-server/docroot,' /etc/apache2/apache2.conf

