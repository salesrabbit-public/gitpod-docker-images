# Gitpod Docker Images

Docker images for remote development environments on Gitpod.

## Building images

Each time you build a new image you'll have to version the tag. Gitpod caches the images on their system, so only new tags will be pulled.

To build an image `cd` into the project directory and run the following commands:

- $`docker build -t <image_tag> -f <project>.dockerfile .`
- $`docker push <image_tag>`

## Image Tags

| Environment | ImageTag |
| ------ | ------ |
| DataLayer | registry.gitlab.com/salesrabbit-public/gitpod-docker-images/data-layer:`<version>` |
| Crux | registry.gitlab.com/salesrabbit-public/gitpod-docker-images/crux:`<version>` |
| ElixirApi | registry.gitlab.com/salesrabbit-public/gitpod-docker-images/elixir-api:`<version>` |
| SRS | registry.gitlab.com/salesrabbit-public/gitpod-docker-images/srs:`<version>` |
| Frontend Platform | registry.gitlab.com/salesrabbit-public/gitpod-docker-images/fp:`<version>` |
